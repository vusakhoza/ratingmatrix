export class Creditor {
  account_number: number;
  name: string = '';
  amount: number;
  date: string = '';
  start_rating: string = '';
  end_rating: string = '';
  complete: boolean = false;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

export class RatingMatrix {}
