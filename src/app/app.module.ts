import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import { HomeComponent }  from './home/home.component';
import { ResultComponent } from './result/result.component';
import { PastResultsComponent } from './past_results/past_results.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './app.routes';
import { TooltipModule, ModalModule, TabsModule } from 'ngx-bootstrap';
// import { MaterialModule } from '@angular/material';

@NgModule({
  imports:      [
    BrowserModule,
    HttpModule,
    routing,
    FormsModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    // MaterialModule,
  ],
  declarations: [ 
    AppComponent,
    HomeComponent,
    ResultComponent,
    PastResultsComponent
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
