import { Component, OnInit, ViewChild } from '@angular/core';
import { CreditorService } from './../services/creditor.service';
import { Creditor, RatingMatrix } from './../models/creditors';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { RouterModule, Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap'

@Component({
//   selector: 'my-app',
  templateUrl: './home.component.html',
  providers: [ModalDirective]

})
export class HomeComponent  {

    @ViewChild('staticModal') public modal:ModalDirective;
    @ViewChild('loadingModal') public load:ModalDirective;
    public errorMessage: string = "";
    public fileList: FileList;

    constructor(
        private http: Http,
        private router: Router,
    ) {}

    file: Object = {
        description: ""
    }

    upload(event:any) {
        this.fileList = event.target.files;
    }

    submit() {
        this.loadStart();
        if( this.file["description"] != "" ) {
            this.makeFileRequest('http://localhost:1337/file/upload/', this.file, this.fileList)
            .then((result) => {
                console.log(result);
            }, (error) => {
                console.error(error);
                this.handleError("There was a network error. Please try again!");
            });
        }else{
            this.handleError("Please enter a short description first.");
        }
    }

    makeFileRequest(url: string, params: any, files: any) {
        return new Promise((resolve, reject) => {
            let that = this;
            let formData: any = new FormData();
            let xhr = new XMLHttpRequest();
            console.log(files);
            
            for(let i = 0; i < files.length; i++) {
                formData.append("file", files[i], files[i].name);
            }
            
            formData.append("description", params.description);
            console.log(formData);

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {//file upload completed
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                        console.log("File upload successful");
                        let result:any = JSON.parse(xhr.response);
                        localStorage.result_link = result["link"]; 
                        localStorage.result_details = that.file["description"] + " - " + result["files"][0]["filename"]; 
                        setTimeout(function() {
                            that.router.navigate(['/past_results']);
                            that.loadStop();
                        }, 2000);
                    } else {
                        reject(xhr.response);
                        console.log("File upload unsuccessful");
                        that.handleError("There was a network error. Please try again!");
                    }
                }
            }
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    }

    handleError(message:string) {
        this.errorMessage = message;
        this.modal.show();
        this.loadStop();
    }

    loadStart(){
        this.load.show();
    }

    loadStop(){
        this.load.hide();
    }

}
