// Import component decorator
import { Component, OnInit } from '@angular/core';
import { CreditorService } from './../services/creditor.service';
import { Creditor, RatingMatrix } from './../models/creditors';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { RouterModule, Router } from '@angular/router';

@Component({
  templateUrl: './past_results.component.html',
  providers: [CreditorService]
})

// Component class
export class PastResultsComponent {

  constructor(
      private http: Http,
      private router: Router,
      private creditorService: CreditorService
  ) { }

  results: any[] = [];

  public ngOnInit() {
    
    this.creditorService
      .getResults()
      .subscribe(
        (results) => {
          console.log(results);
          this.results = results;
        }
      );

  }

  view(link:String, details:String) {
    localStorage.result_link = link; 
    localStorage.result_details = details; 
    this.router.navigate(['/result']);
  }

  reload() {
    this.router.navigateByUrl('/home', true);
    this.router.navigate(["past_results"]);
  }

}