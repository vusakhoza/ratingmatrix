import { Injectable } from '@angular/core';
import { Creditor, RatingMatrix } from './../models/creditors';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Http, Response, RequestOptions } from '@angular/http';


@Injectable()
export class CreditorService {

    constructor(private http: Http) { }
    
    getData(url:any) {
        console.log(url);
        return this.http
        .get('http://localhost:1337' + url)
        .map(response => {
            console.log(response);
            let obj = this.csvTojs(response["_body"]);
            console.log(obj);
            const creditors = obj;
            // return creditors.map((creditor:Creditor) => new Creditor(creditor));
            return creditors;
        })
        .catch(this.handleError);
    }

    getResults(){
        return this.http
        .get('http://localhost:1337/file/results/')
        .map(response => {
            console.log(response);
            console.log(response.json());

            const results = response.json();
            return results;
            // return results.map((result:any) => new Creditor(creditor));
        })
        .catch(this.handleError);
    }

    sortRatings(creditors: any) {
        
        let ratings = ["AA", "A1", "A2", "B1", "B2", "B3", "B4", "C", "D", "E"];
        let completeMatrix = [];

        let matrix: any = [
            { "rating": ["AA", "false"], "total": [0, "false"], "AA": [0, 0], "A1": [0, 0], "A2": [0, 0], "B1": [0, 0], 
                "B2": [0, 0], "B3": [0, 0], "B4": [0, 0], "C": [0, 0], "D": [0, 0], "E": [0, 0] },//AA row
            { "rating": ["A1", "false"], "total": [0, "false"], "AA": [0, 0], "A1": [0, 0], "A2": [0, 0], "B1": [0, 0], 
                "B2": [0, 0], "B3": [0, 0], "B4": [0, 0], "C": [0, 0], "D": [0, 0], "E": [0, 0], },//A1 row
            { "rating": ["A2", "false"], "total": [0, "false"], "AA": [0, 0], "A1": [0, 0], "A2": [0, 0], "B1": [0, 0], 
                "B2": [0, 0], "B3": [0, 0], "B4": [0, 0], "C": [0, 0], "D": [0, 0], "E": [0, 0], },//A2 row
            { "rating": ["B1", "false"], "total": [0, "false"], "AA": [0, 0], "A1": [0, 0], "A2": [0, 0], "B1": [0, 0], 
                "B2": [0, 0], "B3": [0, 0], "B4": [0, 0], "C": [0, 0], "D": [0, 0], "E": [0, 0], },//B1 row
            { "rating": ["B2", "false"], "total": [0, "false"], "AA": [0, 0], "A1": [0, 0], "A2": [0, 0], "B1": [0, 0], 
                "B2": [0, 0], "B3": [0, 0], "B4": [0, 0], "C": [0, 0], "D": [0, 0], "E": [0, 0], },//B2 row
            { "rating": ["B3", "false"], "total": [0, "false"], "AA": [0, 0], "A1": [0, 0], "A2": [0, 0], "B1": [0, 0], 
                "B2": [0, 0], "B3": [0, 0], "B4": [0, 0], "C": [0, 0], "D": [0, 0], "E": [0, 0], },//B3 row
            { "rating": ["B4", "false"], "total": [0, "false"], "AA": [0, 0], "A1": [0, 0], "A2": [0, 0], "B1": [0, 0], 
                "B2": [0, 0], "B3": [0, 0], "B4": [0, 0], "C": [0, 0], "D": [0, 0], "E": [0, 0], },//B4 row
            { "rating": ["C", "false"], "total": [0, "false"], "AA": [0, 0], "A1": [0, 0], "A2": [0, 0], "B1": [0, 0], 
                "B2": [0, 0], "B3": [0, 0], "B4": [0, 0], "C": [0, 0], "D": [0, 0], "E": [0, 0], },//C row
            { "rating": ["D", "false"], "total": [0, "false"], "AA": [0, 0], "A1": [0, 0], "A2": [0, 0], "B1": [0, 0], 
                "B2": [0, 0], "B3": [0, 0], "B4": [0, 0], "C": [0, 0], "D": [0, 0], "E": [0, 0], },//D row
        ]
        console.log(matrix);

        //Calculating and placing start and end ratings
        for( let key in creditors ) {
            for( let i of ratings ) {
                if( creditors[key].start_rating == i ) {
                    let c = creditors[key].start_rating;
                    c = c.trim();// trim white spaces
                    switch (c){
                        case "AA":
                            matrix[0]["total"][0]++;
                            matrix[0][creditors[key]["end_rating"]][0]++;
                            break;
                        case "A1":
                            matrix[1]["total"][0]++;
                            matrix[1][creditors[key]["end_rating"]][0]++;
                            break;
                        case "A2":
                            matrix[2]["total"][0]++;
                            matrix[2][creditors[key]["end_rating"]][0]++;
                            break;
                        case "B1":
                            matrix[3]["total"][0]++;
                            matrix[3][creditors[key]["end_rating"]][0]++;
                            break;
                        case "B2":
                            matrix[4]["total"][0]++;
                            matrix[4][creditors[key]["end_rating"]][0]++;
                            break;
                        case "B3":
                            matrix[5]["total"][0]++;
                            matrix[5][creditors[key]["end_rating"]][0]++;
                            break;
                        case "B4":
                            matrix[6]["total"][0]++;
                            matrix[6][creditors[key]["end_rating"]][0]++;
                            break;
                        case "C":
                            matrix[7]["total"][0]++;
                            matrix[7][creditors[key]["end_rating"]][0]++;
                            break;
                        case "D":
                            // console.log(key);
                            // console.log(creditors[key]);
                            // console.log(creditors[key].start_rating);
                            // console.log(creditors[key].end_rating);
                            // console.log(creditors[key]["end_rating"]);
                            matrix[8]["total"][0]++;
                            matrix[8][creditors[key]["end_rating"]][0]++;
                            break;
                    }
                }
            }
        }

        let percmatrix = matrix;
        for( let key in creditors ) {//sort out percentages
            for( let i=0;i<9;i++ ) {
                let totalAtEnd: number = matrix[i][creditors[key]["end_rating"]][0];
                let totalAtStart: number = matrix[i]["total"][0];
                let percentage = ((totalAtEnd/totalAtStart) * 100).toFixed(2);
                matrix[i][creditors[key]["end_rating"]][1] = (totalAtStart == 0) ? 0 : percentage;
                // console.log( (totalAtStart == 0) ? 0 : percentage );
            }
        }

        completeMatrix = matrix;
        console.log(completeMatrix);
        return completeMatrix;
    }

    private handleError (error: Response | any) {
        console.error('ApiService::handleError', error);
        return Observable.throw(error);
    }

    private csvTojs(csv:any) {
        let lines=csv.split("\n");
        let result = [];
        let headers = lines[0].split(",");

        for(let i=1; i<lines.length; i++) {
            let obj = {};
            let row = lines[i],
            queryIdx = 0,
            startValueIdx = 0,
            idx = 0;
            if (row.trim() === '') { continue; }
            while (idx < row.length) {
            /* if we meet a double quote we skip until the next one */
            let c = row[idx];
            if (c === '"') {
                do { c = row[++idx]; } while (c !== '"' && idx < row.length - 1);
            }
            if (c === ',' || /* handle end of line with no comma */ idx === row.length - 1) {
                /* we've got a value */
                let value = row.substr(startValueIdx, idx - startValueIdx).trim();
                /* skip first double quote */
                if (value[0] === '"') { value = value.substr(1); }
                /* skip last comma */
                if (value[value.length - 1] === ',') { value = value.substr(0, value.length - 1); }
                /* skip last double quote */
                if (value[value.length - 1] === '"') { value = value.substr(0, value.length - 1); }
                let key = headers[queryIdx++];
                obj[key] = value;
                startValueIdx = idx + 1;
            }
            ++idx;
            }
            result.push(obj);
        }
        return result;
    }

}