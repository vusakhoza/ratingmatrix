import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {

    public handleError(t:any) {
        t.errorMessage = "Please enter a short description first.";
        t.modal.show();
    }
    
}
