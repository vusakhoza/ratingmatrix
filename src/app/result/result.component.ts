import { Component, OnInit } from '@angular/core';
import { CreditorService } from './../services/creditor.service';
import { Creditor, RatingMatrix } from './../models/creditors';
import { TooltipModule } from 'ngx-bootstrap';

@Component({
//   selector: 'my-app',
  templateUrl: './result.component.html',
  providers: [CreditorService]

})

export class ResultComponent  {

  creditors: any[] = [];
  result_details: string = "";
  keysGetter: any = Object.keys;
  percmatrix: any[] = [];

  constructor( private creditorService: CreditorService ) {}

  public ngOnInit() {
    this.creditorService
      .getData(localStorage.result_link)
      .subscribe(
        (creditors) => {
          console.log(creditors);
          this.result_details = localStorage.result_details;
          this.creditors = this.creditorService.sortRatings(creditors);
          this.percmatrix = this.creditorService.sortRatings(creditors);
          for( let i=0;i<9;i++ ) {//correct number of columns for percentages tab
              delete this.percmatrix[i]["total"];
          }

        }
      );
  }

}

